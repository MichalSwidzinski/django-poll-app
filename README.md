# Django Poll App
- Training project following the tutorial to practice use of Django.
## Description
- App based on the https://docs.djangoproject.com/en/4.0/intro/tutorial01/ tutorial.
## Getting Started
### Setup
### Dependencies
- Python 3.8
- Additional requirements in requirements.txt
### Authors
- Michał Świdziński